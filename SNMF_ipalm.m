function [ Aout, xt, error, time] = SNMF_ipalm(y,n_epochs, tau, r, Ain, xin)
% Implement PALM for sparse non-negative matrix
% factorization
%      argmin_{A,X} \|Y - AX\|_F^2 
%
%      s.t. \|A_k\|_0 <= tau \forall k, A_{i,j} >=0,  X_{i,j} >= 0
%
%
%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


[n , d] =   size(y);

xi     =    zeros(r,d); 




param.epsilon = tau;

error = zeros(n_epochs, 1);

pn = 5; % number of power iterations

% initialization

 A = Ain;
 
xi = xin;

time = zeros(n_epochs, 1);
t_total = 0;

e0 = 0.5 * ( norm( A * xi - y ,'fro') )^2 ;

xi_a = xi;
A_a = A;

md = zeros(1,r);

for k = 1 : n_epochs 
    
    tic;
    
    a = (k-1)/(k+2); % inertial parameter
    
    xi = xi + a * (xi - xi_a);
    xi_a = xi;
    
    L_A = power_method(A, pn);
  
    
    u  = .9/L_A;
 
    



        grad   =   A'*(A*xi - y); % gradient calculation
        
        xi     =   xi - u*(grad); 

    
        
        

        xi(xi < 0) = 0;           

      L_x = power_method(xi, pn);    
         uy = .9/L_x;    
         
         
         A = A + a * (A - A_a);
         
         A_a = A;
         
        A = A - uy * ((A*xi - y)*xi');
        
     

             B = sort(abs(A), 1, 'descend');
            md = B(tau,:);
       for q = 1:r % hard - tresholding

         
           % md(q) = kthvalue(-A(:,q),tau);
         
         
            A(:,q) = wthresh(A(:,q),'h',md(q));
       end
        A(A<0) = 0;                 

  
    
    
    

    t1 = toc;
    t_total = t_total + t1;
    time(k) = t_total;    
    error(k) = 0.5 * ( norm( A * xi - y ,'fro') )^2 ;

end

xt = xi; % output

Aout = A;


error = [e0; error];


end









