function [ Aout, xt, error, time ] = SNMF_SPRING_SARAH_e(y,sr,n_epochs, tau, r, Ain, xin)
% Implement SPRING-SARAH for sparse non-negative matrix
% factorization
%      argmin_{A,X} \|Y - AX\|_F^2 
%
%      s.t. \|A_k\|_0 <= tau \forall k, A_{i,j} >=0,  X_{i,j} >= 0
%
%
%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



[n , d] =   size(y);

xi     =    zeros(r,d); 

m       =  floor( n / sr ); 
m2 = floor( d/sr );








pn = 5;




param.epsilon = tau;

error = zeros(n_epochs,1);


 A = Ain;
 
xi = xin;

At  =  A;

A_tmp = A;



xi_tmp = xi;
xt  = xi;

t = 1;

L_A = 0;
L_x = 0;

time = zeros(n_epochs, 1);
t_total = 0;


e0 = 0.5 * ( norm( A * xi - y ,'fro') )^2 ;%+ tau * norm(xi(:), 1) + tau2 * norm(A(:), 1);

md = zeros(1,r);

for k = 1 : n_epochs % every epoch
    
    tic;
    
  if k > 1%10  % full gradient at each outer loop
    
    grad =   At'*(At*xt - y)./sr; 
    
    L_A = power_method(At, pn)/sr;
    
    u = .5/L_A;
    
    xi_tmp = xt;
    xi = xt - u * grad;
    
     xi(xi < 0) = 0;         
    
    
    grad2 =   (At*xi - y)*xi'./sr;
        L_x = power_method(xi, pn)/sr;

   
     uy = .5/L_x; %uby;%     
     
     A_tmp = At;
     A = At - uy * (grad2); 
       for q = 1:r % hard - tresholding
          
           Aq = A(:, q);
          [~, idxq] = sort(Aq, 1, 'ascend');
          Aq(idxq(1: (n - tau))) = 0;
          A(:, q) = Aq;

       end         
           
    
    
        A(A<0) = 0;  
        
  end
     
    idxb  =  randperm(sr,sr);
    idxb2 =  randperm(sr,sr);
    idx    =   (1 + (idxb(1)-1)*m): (idxb(1)*m);
    idx2    =   (1 + (idxb2(1)-1)*m2): (idxb2(1)*m2);
    
    L_A = power_method(At, pn)./sr;
    L_x = power_method(xt, pn)./sr;
    
    for i = 1 : sr  % inner loop

        if idxb(i) == sr
            
         idx    =  (1 + (idxb(i)-1)*m): n;%(idxb(i)*m);  
        else
        
         idx    =  (1 + (idxb(i)-1)*m): (idxb(i)*m); %randperm(n, m);%
        
        end
        As     =   A(idx,:);%.*sqrt(n/m);
        Ass    =   A_tmp(idx, :);
        ys     =   y(idx,:);%.*sqrt(n/m);
        
         L_A = power_method(A(idx,:), pn);%max(power_method(A(idx,:), pn) , L_A);
 

        u  = 1/L_A/2;
  

          
        if k <= 1%10
        
         grad   =  As'*(As*xi - ys); % SG for first few epochs
        
        else
         grad   =  As'*(As*xi - ys) - Ass'*(Ass*xi_tmp - ys) + grad;    % SARAH update
        end
        
        xi_tmp = xi;
        
        xi     =   xi - u*(grad); 
        
        
        xi(xi < 0) = 0;            
 
   
        if idxb2(i) == sr
            
         idx    =  (1 + (idxb2(i)-1)*m2): d; 
        else
        
         idx    =  (1 + (idxb2(i)-1)*m2): (idxb2(i)*m2); 
        
        end   
        
    
        xi2     =   xi(:,idx);
        xii2    = xi_tmp(:,idx);
        ys2     =   y(:,idx);
        
       L_x = power_method(xi(:, idx), pn);

       
        uy = 1/L_x/2; 

     if k <= 1%10
        
        grad2   =  (xi2*(A*xi2 - ys2)')' ; % SG for first few epochs
        
     else
         
         grad2   = (A*xi2 - ys2)*xi2' - (A_tmp*xii2 - ys2)*xii2' + grad2;%SARAH update
     end
        
        A_tmp = A;
        
        A = A - uy * (grad2); 
        
        
       
     
              B = sort(abs(A), 1, 'descend');
              md = B(tau,:);
                
       for q = 1:r % hard - tresholding


         
            A(:,q) = wthresh(A(:,q),'h',md(q));
%             Aa = A(:,q);
%             Am = wthresh(Aa,'h',md(q));
% 
%             A(:,q) = Am;
       end  

        A(A<0) = 0;          
        
    end
    At  =  A;
    
    A_tmp = A;
    
    xt  = xi;
    
    x_tmp = xi;
    

    
    L_A = 0;
    L_x = 0;
    

    t1 = toc;
    t_total = t_total + t1;
    time(k) = t_total;    
     error(k) = 0.5 * ( norm( A * xi - y ,'fro') )^2;% + tau * norm(xi(:), 1) + tau2 * norm(A(:), 1);

end

xt = xi; % output

Aout = A;

error = [e0; error];

%err    =   [(norm(x_truth))^2, err];

end


%  v  = sign(c1 .* v) .* (max( abs(c1 .* v) -  (tau * (1/(3*L)) * c2) .* I_tmp, 0) );











